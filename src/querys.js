import gql from 'graphql-tag';

export const GET_NEWS = gql`
  query {
    news {
      edges {
        node {
          id
          title
          body
          author {
            username          
          }
          publicationDate  
        }
      }
    }
  }
`;

export const GET_PORTALS = gql`
  query {
    portals {
      edges {
        node {
          id
          name
        }
      }
    }
  }
`