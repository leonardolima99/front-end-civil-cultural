import React from 'react'

export default function Comment(props) {
  return (
    <React.Fragment>
      <div className="row mb-4">
        <div className="col public box-create">
          <div className="tuk">
            <span className="title">Faça um comentário:</span>
          </div>
          <textarea className="comment" placeholder="Escreva seu comentário aqui!"></textarea>
          <button className="btn btn-primary btn-block">Comentar</button>
        </div>

      </div>
    </React.Fragment>
  )
}