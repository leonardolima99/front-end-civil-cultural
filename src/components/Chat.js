import React from 'react';
import { Link } from 'react-router-dom'
import Condition from './Condition';

function Sidebar(props) {
	return (
		<Condition test={props.visible}>
			<nav id="sidebar" className="sticky chat">
				<div className="sidebar-header">
					Chat
				</div>

				<div className="line"></div>

				<ul className="list-unstyled components ml-2 mr-2">
					<li className="active mb-4">
						<strong>Administrador:</strong> <br/>
						Fulano de Tal <br/>
					</li>
					<li className="mb-4">
						<strong>
							Peritos:
						</strong> <br/>
						Fulano de tal <br/>
						Fulano de tal <br/>
					</li>
					<li className="mb-4">
						<strong>Tempo online:</strong> <br/>
						{Date.now()}
					</li>
					<li className="mb-4">
						
					</li>
				</ul>
			</nav>
		</Condition>
		
	);
}

export default Sidebar;
