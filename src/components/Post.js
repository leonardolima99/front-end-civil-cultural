import React from 'react'

import { Mutation } from 'react-apollo'
import { DELETE_NEWS } from '../mutations'

import { success, fail } from '../utils/toast_msg'

function getDate(value) {
  value = new Date(value)
  return value.toLocaleDateString()
}

export default function Posts(props) {
  const { data } = props
  return (
    <React.Fragment>
      <div className="row mb-4">
        <div className="col">
          <div className="news">
            <div className="title">{data.title}</div>
            <div className="small">
              <span className="date">em {getDate(data.publicationDate)}</span>
              <span> | 
                por&nbsp;
                <a href="#!" className="author">
                  {data.author.username}
                </a>
              </span>
            </div>
          </div>
          <div className="news-prev">
            <div className="news-body" dangerouslySetInnerHTML={{__html: data.body}}>
            </div>
            {/* <img src={image} className="cover" height="150" /> */}
          </div>
          <div className="actions">
            <button className="btn btn-primary mr-2 btn-sm">Ver mais</button>
            <Mutation
              mutation={DELETE_NEWS}
              variables={{id: data.id}}
              // onCompleted={() => success('Notícia apagada.', 1000, '/')}
              onCompleted={() => success('Notícia apagada.', 1000)}
              onError={(error) => fail(error.message, 1000)}
              >
                {mutation => (
                  <button 
                    onClick={mutation}
                    className="btn btn-danger btn-sm"
                    >Apagar</button>
                )}
              </Mutation>
          </div>
          
        </div>
      </div>
    </React.Fragment>
  )
}