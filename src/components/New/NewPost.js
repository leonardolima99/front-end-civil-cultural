import React, { Component, Fragment } from 'react'
import { withRouter } from 'react-router'

import Loading from './../Loading/Loading'

import { Query } from 'react-apollo'
import { Mutation } from 'react-apollo'

import { GET_PORTALS } from '../../querys'
import { CREATE_NEWS, CREATE_PORTAL } from '../../mutations'

import { success, fail } from '../../utils/toast_msg'

import './NewPost.css'

class NewPost extends Component {
  state = {
    title: '',
    body: '',
    portal: '',
    name: ''
  }


  render() {
    const { isActive, addNews } = this.props
    const { title, body, portal, name } = this.state
    const tabs = document.querySelectorAll('input[name="tab-control"]')
    console.log(tabs)
    return (
      <Fragment>
        <div className="tabs mb-4">
          <input type="radio" id="tab1" name="tab-control" defaultChecked />
          <input type="radio" id="tab2" name="tab-control" />
          <input type="radio" id="tab3" name="tab-control" />
          <input type="radio" id="tab4" name="tab-control" />
          <ul>
            <li title="Criar Notícia">
              <label htmlFor="tab1" className="tab">
                <strong>
                  NOTÍCIA
                </strong>
              </label>
            </li>
            <li title="Criar Portal">
              <label htmlFor="tab2" className="tab">
                <strong>
                  PORTAL
                </strong>
              </label>
            </li>
            <li title="Criar Tópico">
              <label htmlFor="tab3" className="tab">
                <strong>
                  TÓPICO
                </strong>
              </label>
            </li>
            <li title="Criar Artigo">
              <label htmlFor="tab4" className="tab">
                <strong>
                  ARTIGO
                </strong>
              </label>
            </li>
          </ul>
          
          <div className="line">
            <div className="slider">
              <div className="indicator"></div>
            </div>    
          </div>
          <div className="content">
            
            <section>
              <input 
                type="text"
                className="title-news" 
                placeholder="Título da notícia aqui." 
                onChange={(e) => {this.setState({ title: e.target.value })}}
                defaultValue={""} 
              />
              <div 
                className="content-editable"
                contentEditable="true"
                onInput={(e) => {this.setState({ body: e.target.innerHTML })}}
                data-placeholder="Conteúdo da notícia aqui."
                ></div>
              <Query query={GET_PORTALS}>
                {({ loading, error, data }) => {
                  if (loading) return (<Loading size={1} />)
                  if (error) return `${error.message}`

                  return (
                    <select 
                      name="portal" 
                      defaultValue=""
                      onChange={(e) => {this.setState({ portal: e.target.value })}}>
                      <option value=""> ---- Escolha um portal ---- </option>
                      {data.portals.edges.map((docs) => {
                        return (
                          <option 
                            key={docs.node.id} 
                            value={docs.node.id}
                            >{docs.node.name}</option>
                        )
                      }
                      )}
                    </select>
                  )
                }}
              </Query>
              <div className="actions">
              <Mutation
                  mutation={CREATE_NEWS}
                  variables={{ title, body, portal }}
                  onCompleted={() => success('Notícia criada com sucesso.', 1000, '/')}
                  onError={(error) => fail(error.message, 1000)}
                  >
                    {(mutation) => (
                      <button 
                        className="button button-primary mr-1"
                        data-dismiss="modal"
                        onClick={mutation}
                        disabled={!title || !body || !portal}
                        >Criar</button>
                    )}
                </Mutation>
                <button 
                  type="button" 
                  className="button"
                  >Cancelar</button>
              </div>
            </section>
            
            <section>
              <input 
                type="text"
                className="title-news" 
                placeholder="Nome do portal." 
                onChange={(e) => {this.setState({ name: e.target.value })}}
                defaultValue={""} 
              />
              {/* <div className="form-group font-light mt-3">
                <input type="checkbox" id="isPublic" />
                <label htmlFor="isPublic">É um portal público?</label>
              </div> */}
              
              <div className="actions">
              <Mutation
                  mutation={CREATE_PORTAL}
                  variables={{ name }}
                  onCompleted={() => success('Portal criado com sucesso.', 1000, '/')}
                  onError={(error) => fail(error.message, 1000)}
                  >
                    {(mutation) => (
                      <button 
                        className="button button-primary mr-1"
                        data-dismiss="modal"
                        onClick={mutation}
                        disabled={!name}
                        >Criar</button>
                    )}
                </Mutation>
                <button 
                  type="button" 
                  className="button"
                  >Cancelar</button>
              </div>
            </section>
            
            
          </div>
        </div>
        
      </Fragment>
    )
  }
}

export default withRouter(NewPost)