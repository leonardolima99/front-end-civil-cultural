import React, { Component, Fragment } from 'react'
import { withRouter } from 'react-router'

import Loading from './Loading/Loading'

import { Query } from 'react-apollo'
import { Mutation } from 'react-apollo'

import { GET_PORTALS } from '../querys'
import { CREATE_NEWS } from '../mutations'

import { success, fail } from '../utils/toast_msg'

class NewPost extends Component {
  state = {
    title: '',
    body: '',
    portal: ''
  }


  render() {
    const { isActive, addNews } = this.props
    const { title, body, portal } = this.state
    return (
      <Fragment>
        <div className="tabs">
          <input type="radio" id="tab1" name="tab-control" checked />
          <input type="radio" id="tab2" name="tab-control" />
          <input type="radio" id="tab3" name="tab-control" />
          <input type="radio" id="tab4" name="tab-control" />
          <ul>
            <li title="Criar Notícia">
              <label htmlFor="tab1" className="tab">
                <strong>
                  NOTÍCIA
                </strong>
              </label>
            </li>
            <li title="Criar Portal">
              <label htmlFor="tab2" className="tab">
                <strong>
                  PORTAL
                </strong>
              </label>
            </li>
            <li title="Criar Tópico">
              <label htmlFor="tab3" className="tab">
                <strong>
                  TÓPICO
                </strong>
              </label>
            </li>
            <li title="Criar Artigo">
              <label htmlFor="tab4" className="tab">
                <strong>
                  ARTIGO
                </strong>
              </label>
            </li>
          </ul>
        </div>
              <div className="modal-body">
                <input 
                  type="text"
                  className="title-news" 
                  placeholder="Título da notícia aqui." 
                  onChange={(e) => {this.setState({ title: e.target.value })}}
                  defaultValue={""} 
                />
                <div 
                  className="content-editable"
                  contentEditable="true"
                  onInput={(e) => {this.setState({ body: e.target.innerHTML })}}
                  data-placeholder="Conteúdo da notícia aqui."
                  ></div>
                <Query query={GET_PORTALS}>
                  {({ loading, error, data }) => {
                    if (loading) return (<Loading size={1} />)
                    if (error) return `${error.message}`

                    return (
                      <select 
                        name="portal" 
                        defaultValue=""
                        onChange={(e) => {this.setState({ portal: e.target.value })}}>
                        <option> ---- Escolha um portal ---- </option>
                        {data.portals.edges.map((docs) => {
                          return (
                            <option 
                              key={docs.node.id} 
                              value={docs.node.id}
                              >{docs.node.name}</option>
                          )
                        }
                        )}
                      </select>
                    )
                  }}
                </Query>
                <div className="actions">
                  
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <Mutation
                    mutation={CREATE_NEWS}
                    variables={{ title, body, portal }}
                    onCompleted={() => success('Notícia criada com sucesso.', 1000, '/')}
                    onError={(error) => fail(error.message, 1000)}
                    >
                      {(mutation) => (
                        <button 
                          className="btn btn-primary"
                          data-dismiss="modal"
                          onClick={mutation}
                          disabled={!title || !body || !portal}
                          >Publicar</button>
                      )}
                  </Mutation>
              </div>
            {/* </div> */}
        {/* </div> */}
        {/* </div> */}
        
      </Fragment>
    )
  }
}

export default withRouter(NewPost)