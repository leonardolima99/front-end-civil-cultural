import React, { Component, Fragment } from 'react';

import NewPost from '../New/NewPost'
import ListPosts from '../ListPosts'

export default class Home extends Component {
  state = {
    active: false,
    reloadNews: true
  }
  
  setReloadNews() {
    return this.setState({ reloadNews: true })
  }

  render() {
    const { active, reloadNews } = this.state
    console.log(reloadNews)
    return (
      <Fragment>
        <div className="container">
          
          <NewPost
            className="mb-4"
            isActive={active}
            setReloadNews={this.setReloadNews}
          />
          {/* Listagem de posts */}
          <ListPosts isReload={this.reloadNews} />
          
        </div>
      </Fragment>
    )
  }
}
