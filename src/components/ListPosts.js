import React, { Component, Fragment } from 'react'

import Loading from './Loading/Loading'

import { Query } from 'react-apollo'
import { GET_NEWS } from '../querys'

import Post from './Post'

export default class ListPosts extends Component {
  render() {
    return (    
      <Fragment>
        <Query query={GET_NEWS}>
          {({ loading, error, data }) => {
            if (loading) return (<Loading size={5} />)
            if (error) return `Error! ${error.message}`
            return data.news.edges.map((docs) => {
              return (
                <Post 
                  key={docs['node'].id}
                  data={docs['node']} 
                  />
              )
            })
          }}
        </Query>
      </Fragment>
    )
  }
}